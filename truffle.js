
var HDWalletProvider = require('truffle-hdwallet-provider');
var mnemonic = 'castle recall moon mad search innocent aerobic feel equal master emotion robot';

module.exports = {
  networks: {

    development: {

      host: 'localhost',
      port: 8545,
      network_id: '*'
    },
    rinkeby: {

      provider: function(){

        return new HDWalletProvider(mnemonic, 'https://rinkeby.infura.io/uQvtvfd93tOXueXZNbd9', 0);
      },
      network_id: 3,
      gas: 4000000
    },
    remoteganache: {

      provider: function(){

        return new HDWalletProvider(mnemonic, 'http://169.60.157.140:4321', 0);
      },
      network_id: 4321,
      gas: 4000000
    }
  }
};
